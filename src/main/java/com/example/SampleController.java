/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("sample")
@CrossOrigin
public class SampleController {

    public SampleController() {
    }

    /**
     *
     * @param foo
     * @param request optional to have this. I need this to get a header value.
     * @return
     * @throws Exception
     */
    @CrossOrigin
    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> add(@RequestParam("foo") String foo, HttpServletRequest request) throws Exception {
        final Map<String, Object> responseMap = new HashMap<>();

        String remoteUserHeaderValue = request.getHeader("REMOTE_VALUE");

        System.err.println("[REMOTE_VALUE]"+remoteUserHeaderValue+" with request contant: "+foo);
        
        responseMap.put("status", "success");
        responseMap.put("message", "Hello "+foo);
        return responseMap;
    }
}
